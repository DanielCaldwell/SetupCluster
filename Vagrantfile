BOX_IMAGE = "ubuntu/bionic64" # DEC: Upgraded to this 18.04.03 Image. It worked :D
# BOX_IMAGE = "ubuntu/xenial64" # DEC: This is Ubuntu 16.04. I downgraded from the 18.04
# BOX_IMAGE = "bento/ubuntu-18.04"  - DEC: Didn't work
BOX_TIMEOUT = 360
NODE_COUNT = 6
BASTION_COUNT = 2

Vagrant.configure("2") do |config|

   (1..NODE_COUNT).each do |i|
      config.vm.define "node-#{i}" do |box_node|
         # need to run: vagrant plugin install vagrant-disksize
         box_node.disksize.size = '50GB'
         box_node.vm.boot_timeout = BOX_TIMEOUT
         box_node.vm.box = BOX_IMAGE
         box_node.vm.hostname = "node-#{i}"
         box_node.vm.network :private_network, ip: "10.0.0.#{i+20}"
     
         disk_2 = "./node-#{i}-disk-2.vdi"
         disk_3 = "./node-#{i}-disk-3.vdi"
 
         box_node.vm.provider "virtualbox" do |vbox_node|
            vbox_node.name = "node-#{i}"
            vbox_node.memory = 10000
            vbox_node.cpus = 3
            unless File.exists?(disk_2)
               vbox_node.customize ['createhd', '--filename', disk_2, '--variant', 'Fixed', '--size', 100*1024]
            end
            unless File.exists?(disk_3)
               vbox_node.customize ['createhd', '--filename', disk_3, '--variant', 'Fixed', '--size', 100*1024]
            end
            vbox_node.customize ['storageattach', :id, '--storagectl', 'SCSI', '--port', 3, '--device', 0, '--type', 'hdd', '--medium', disk_2]
            vbox_node.customize ['storageattach', :id, '--storagectl', 'SCSI', '--port', 4, '--device', 0, '--type', 'hdd', '--medium', disk_3]


         end
         
      end
   end 

   (1..BASTION_COUNT).each do |i|
      config.vm.define "bastion-#{i}" do |box_bastion|
         box_bastion.vm.boot_timeout = BOX_TIMEOUT
         box_bastion.vm.box = BOX_IMAGE
         box_bastion.vm.hostname = "bastion-#{i}"
         
         # bastion hosts will start with 10.0.0.2 ...   
         box_bastion.vm.network :private_network, ip: "10.0.0.#{i+10}"

         box_bastion.vm.provider "virtualbox" do |vbox_bastion|
            vbox_bastion.name = "bastion-#{i}"
            vbox_bastion.memory = 1000
            vbox_bastion.cpus = 1 
         end 
      end
   end

   config.vm.define "router" do |box_router|
      box_router.vm.boot_timeout = BOX_TIMEOUT
      box_router.vm.box = BOX_IMAGE
      box_router.vm.hostname = "router"

      # the router will be hard coded at 10.0.0.2 ...   
      box_router.vm.network :private_network, ip: "10.0.0.2"
      box_router.vm.network "forwarded_port", guest:80, host:1080

      box_router.vm.provider "virtualbox" do |vbox_router|
         vbox_router.name = "router"
         vbox_router.memory = 1000
         vbox_router.cpus = 1
      end
   end

   config.vm.provision "ansible" do |ansible|
     ansible.verbose = "vv"
     ansible.become = true
     ansible.become_user = "root"
     ansible.raw_arguments = ["-e", "@local.vars", "--vault-password-file=vault.pwf"]
     ansible.playbook = "playbook.yml"
     # ansible.ask_vault_pass = true
   end

end

